from django.test import TestCase, Client
from django.apps import apps
from .apps import DashboardConfig
from django.contrib.auth.models import User
import requests
# Create your tests here.

class DashboardConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(DashboardConfig.name, 'Dashboard')
        self.assertEqual(apps.get_app_config('Dashboard').name, 'Dashboard')

class DashboardTestCase(TestCase):
    def test_url_status_200(self):
        response = Client().get('/')
        self.assertEqual(200, response.status_code)

    def test_template_dashboard(self):
       response = Client().get('/')
       self.assertTemplateUsed(response, 'dashboard.html')
    
    def test_view_dashboard(self):
        response = Client().get('/')
        html_response = response.content.decode ('utf8')
        self.assertIn("Find A Book", html_response)

    def test_register_page_exist(self):
    	response = Client().get('/register/')
    	self.assertEqual(200,response.status_code)

    def test_login_page_exist(self):
    	response = Client().get('/login/')
    	self.assertEqual(200,response.status_code)

    def test_logout_redirect_page(self):
    	response = Client().get('/logout/')
    	self.assertEqual(302,response.status_code)

    def setUp(self):
    	self.credentials = {'username': 'testuser','password': 'secret'}
    	User.objects.create_user(**self.credentials)

    def test_login(self):
    	response = self.client.post('/login/', self.credentials, follow=True)
    	self.assertTrue(response.context['user'].is_active)

    def test_parseData(self):
    	keyword = self.client.get('q')
    	url = 'https://www.googleapis.com/books/v1/volumes?q=%s' %keyword
    	request_data = requests.get(url)
    	self.assertEqual(200,request_data.status_code)
