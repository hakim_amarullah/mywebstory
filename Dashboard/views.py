from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib import messages
from django.http import JsonResponse
import requests
import json
from django.contrib.auth.decorators import login_required
# Create your views here.
def dashboard(request):
	return render (request, 'dashboard.html',{})

@login_required
def parseData(request):
	keyword = request.GET['q']
	url = 'https://www.googleapis.com/books/v1/volumes?q=%s' %keyword
	request_data = requests.get(url)
	data = json.loads(request_data.content)
	return JsonResponse(data, safe=False)

def register(request):
	form = CreateUserForm()

	if request.method == "POST":
		form= CreateUserForm(request.POST)
		if form.is_valid():
			form.save()
			messages.info(request,"Account created, login now")
			return redirect ('/login')

	context = {'form': form}
	return render (request, 'register.html',context)

def login(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(request, username=username, password=password)

		if user is not None:
			auth_login(request, user)
			request.session['name'] = username
			request.session['password'] = password
			return redirect('/')

		else:
			messages.warning(request,"Username or Password Is Invalid")
	context ={}
	return render(request,'login.html',context)

def logout(request):
	auth_logout(request)
	try :
		del request.session['name']
		del request.session['password']
	except KeyError:
		pass
	return redirect('/')


